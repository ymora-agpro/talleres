package cl.agpro.talleres;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import cl.agpro.talleres.webservice.MyFile;
import cl.agpro.talleres.workshop.WorkshopContent;

public class HomeActivity extends MyActivity
        implements NavigationView.OnNavigationItemSelectedListener, WorkshopsTeacherFragment.OnListFragmentInteractionListener {

    private static final String TAG = "HomeActivity";
    String type;

    AccountFragment accountFragment;

    ImageView imageView;

    public MyFile myFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences prefs = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        type = prefs.getString("type", "");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (type.equals("caja"))
            navigationView.getMenu().findItem(R.id.nav_my_account).setVisible(false);

        navigationView.setNavigationItemSelectedListener(this);

        // Load name and image
        View view = navigationView.getHeaderView(0);

        TextView textView = (TextView) view.findViewById(R.id.tv_user_name);
        String name = prefs.getString("name", "");
        textView.setText(name);

        String image = prefs.getString("image", "");

        imageView = (ImageView) view.findViewById(R.id.imageView);
        if (!image.isEmpty())
            new DownloadImageTask(imageView)
                    .execute(image);

        // Checked and show Worshops
        navigationView.getMenu().findItem(R.id.nav_my_workshops).setChecked(true);
        showWorkshops();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            questionExit();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        boolean fragmentTransaction = false;
        String title = "";
        Fragment fragment = null;

        if (id == R.id.nav_my_account) {
            accountFragment = new AccountFragment();
            fragmentTransaction = true;
            title = getString(R.string.nav_my_account);

            fragment = accountFragment;
        } else if (id == R.id.nav_my_workshops) {
            if (type.equals("profe")) {
                fragment = new WorkshopsTeacherFragment().newInstance(1);
            } else {
                WorkshopsFragment tallerFragment = WorkshopsFragment.newInstance("1", "2");
                tallerFragment.setActivity(this);
                fragment = tallerFragment;
            }

            fragmentTransaction = true;
            title = getString(R.string.nav_my_workshops);
        } else if (id == R.id.nav_logout) {
            closeSession();
        }

        if (fragmentTransaction) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit();

            getSupportActionBar().setTitle(title.toString());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String name = "foto";

            //Uri myUri;
            Bitmap bitmap = null;
            if (requestCode == REQUEST_ACCOUNT_GALLERY) {
                Uri myUri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), myUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Server
                myFile = new MyFile(name, new File(getRealPathFromUri(this, myUri)));

                Toast.makeText(this, getString(R.string.upload_image_success), Toast.LENGTH_SHORT).show();
            } else {
                // Server
                //File image = new File(mCurrentPhotoPath);
                //myFile = new MyFile(name, image);

                myFile = new MyFile(name, imageCamera);

                bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                //myUri = Uri.parse(mCurrentPhotoPath);
            }

            //try {
            bitmap = getCircularBitmap(bitmap);

            imageView.setImageBitmap(bitmap);
            accountFragment.setPhoto(bitmap);
            /*} catch (IOException e) {
                e.printStackTrace();
            }*/
        }
    }

    private void showWorkshops() {
        Fragment fragment = null;

        if (type.equals("profe")) {
            fragment = new WorkshopsTeacherFragment().newInstance(1);
        } else {
            WorkshopsFragment tallerFragment = WorkshopsFragment.newInstance("1", "2");
            tallerFragment.setActivity(this);
            fragment = tallerFragment;
        }

        String title = getString(R.string.nav_my_workshops);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.item_detail_container, fragment)
                .commit();

        getSupportActionBar().setTitle(title.toString());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onListFragmentInteraction(WorkshopContent.DummyItem item) {
    }
}
