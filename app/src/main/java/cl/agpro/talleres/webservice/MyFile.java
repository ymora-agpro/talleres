package cl.agpro.talleres.webservice;

import java.io.File;

/**
 * Created by yerkomora on 06-05-16.
 */
public class MyFile {

    private String name;
    private File value;

    public MyFile(String name, File value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public File getValue() {
        return value;
    }
}
