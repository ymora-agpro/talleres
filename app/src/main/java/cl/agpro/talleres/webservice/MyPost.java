package cl.agpro.talleres.webservice;

/**
 * Created by yerkomora on 06-05-16.
 */
public class MyPost {

    private String name, value;

    public MyPost(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
