package cl.agpro.talleres.person;

import java.util.ArrayList;
import java.util.List;

public class PersonContent extends Person {

    public static final List<PersonItem> ITEMS_P = new ArrayList<>();

    public PersonContent(String id_taller, String tipo) {
        super(id_taller, tipo);
    }

    @Override
    protected void addItem(PersonItem item) {
        //if (ITEM_MAP.get(item.id) == null) {
        ITEMS_P.add(item);
        ITEM_MAP.put(item.id, item);
        //}
    }

    @Override
    protected Boolean ItemsIsEmpty() {
            return ITEMS_P.isEmpty();
    }

    @Override
    protected void setItemAll() {
    }
}