package cl.agpro.talleres.person;

import java.util.ArrayList;
import java.util.List;

public class RegisterContent extends Person {

    public static final List<PersonItem> ITEMS_RT = new ArrayList<PersonItem>();
    public static final List<PersonItem> ITEMS_RT_ALL = new ArrayList<PersonItem>();

    public RegisterContent(String id_taller, String tipo) {
        super(id_taller, tipo);
    }

    @Override
    protected void addItem(PersonItem item) {
        ITEMS_RT.add(item);
        ITEM_MAP.put(item.id, item);
    }

    @Override
    protected Boolean ItemsIsEmpty() {
        return ITEMS_RT.isEmpty();
    }

    @Override
    protected void setItemAll() {
        ITEMS_RT_ALL.addAll(ITEMS_RT);
    }
}
