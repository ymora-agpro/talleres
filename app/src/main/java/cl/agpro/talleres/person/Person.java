package cl.agpro.talleres.person;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import cl.agpro.talleres.HomeActivity;


/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public abstract class Person {
    /**
     * An array of sample (dummy) items.
     */
    //public static final List<PersonItem> ITEMS = new ArrayList<PersonItem>();

    HomeActivity homeActivity;

    private static final String TAG = "Person";

    private GetDataAssitance getDataAssitance;
    private GetDataRegister getDataRegister;

    /**
     * A map of sample (dummy) items, by ID.
     */
    public final Map<Integer, PersonItem> ITEM_MAP = new HashMap<>();

    private String id_taller, tipo;

    private OnListFragmentInteractionListener mListener = null;

    public Person(String id_taller, String tipo) {
        this.id_taller = id_taller;
        this.tipo = tipo;
        Log.d(TAG, tipo);

        if (ItemsIsEmpty())
            switch (this.tipo) {
                case "1":
                    getDataAssistance();
                    break;
                case "2":
                    getDataRegister();
                    break;
                default:
                    getDataRegistered();
                    break;
            }
    }

    protected abstract void addItem(PersonItem item);

    protected abstract Boolean ItemsIsEmpty();

    protected abstract void setItemAll();

    private static PersonItem createDummyItem(int id, String name, String rut, String ficha, String inscrito) {
        return new PersonItem(id, name, rut, ficha, inscrito);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class PersonItem {
        public final Integer id;
        public final String name;
        public final String rut;
        public final String ficha;
        public final String inscrito;

        public Boolean assist = false;

        public PersonItem(int id, String name, String rut, String ficha, String inscrito) {
            this.id = id;
            this.name = name;
            this.rut = rut;
            this.ficha = ficha;
            this.inscrito = inscrito;
        }

        @Override
        public String toString() {
            return rut;
        }
    }

    // AsyncTask

    // Assistance
    public void getDataAssistance() {
        getDataAssitance = new GetDataAssitance();
        getDataAssitance.execute(); //OBTENGO DATA DE WS
    }

    private class GetDataAssitance extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {

                Log.d("URL ", homeActivity.URL_WS + "/get_list_inscritos_taller.php?id_taller=" + id_taller);
                url = new URL(homeActivity.URL_WS + "/get_list_inscritos_taller.php?id_taller=" + id_taller);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsAssitance(result);
        }
    }

    private void getResultsAssitance(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("inscritos");

            // Add some sample items.
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);

                String name = row.getString("nombre");
                String rut = row.getString("rut");
                int id = row.getInt("id");
                String ficha = row.getString("ficha");

                addItem(createDummyItem(id, name, rut, ficha, ""));
            }

            if (mListener != null)
                mListener.onListFragmentInteraction();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Register
    public void getDataRegister() {
        getDataRegister = new GetDataRegister();
        getDataRegister.execute(); //OBTENGO DATA DE WS
    }

    private class GetDataRegister extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                // get_list_inscritos_total_taller.php?taller_id=&ciudad=&filtro=
                String filtro = "";

                String Url = homeActivity.URL_WS + "/get_list_inscritos_total_taller.php?taller_id=" + id_taller + "&filtro=" + filtro;
                Log.d("URL ", Url);
                url = new URL(Url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsRegister(result);
        }
    }

    private void getResultsRegister(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("inscritos");

            // Add some sample items.
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);

                String name = row.getString("nombre");
                String rut = row.getString("rut");
                int id = row.getInt("id");
                String inscrito = row.getString("inscrito");

                Log.d("inscrito", i + " : " + inscrito);

                addItem(createDummyItem(id, name, rut, "", inscrito));
            }

            if (mListener != null)
                mListener.onListFragmentInteraction();

            setItemAll();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Registered
    public void getDataRegistered() {
        new GetDataRegistered().execute();
    }

    private class GetDataRegistered extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", homeActivity.URL_WS + "/get_list_inscritos_taller.php?id_taller=" + id_taller);
                url = new URL(homeActivity.URL_WS + "/get_list_inscritos_taller.php?id_taller=" + id_taller);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsRegistered(result);
        }
    }

    private void getResultsRegistered(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("inscritos");

            // Add some sample items.
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);

                String name = row.getString("nombre");
                String rut = row.getString("rut");
                int id = row.getInt("id");
                String ficha = row.getString("ficha");

                addItem(createDummyItem(id, name, rut, ficha, ""));
            }

            if (mListener != null)
                mListener.onListFragmentInteraction();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Interface
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction();
    }

    public void setOnListFragmentInteractionListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }
}
