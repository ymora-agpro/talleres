package cl.agpro.talleres.person;

import java.util.ArrayList;
import java.util.List;

public class RegisteredContent extends Person {

    public static final List<PersonItem> ITEMS_RD = new ArrayList<PersonItem>();

    public RegisteredContent(String id_taller, String tipo) {
        super(id_taller, tipo);
    }

    @Override
    protected void addItem(PersonItem item) {
        ITEMS_RD.add(item);
        ITEM_MAP.put(item.id, item);
    }

    @Override
    protected Boolean ItemsIsEmpty() {
        return ITEMS_RD.isEmpty();
    }

    @Override
    protected void setItemAll() {
    }
}