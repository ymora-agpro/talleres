package cl.agpro.talleres;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class UserEditActivity extends MyActivity {
    private int user_id;
    private String taller_id, tipo;

    String rut, codigo, nombre, apellido, sucursal;
    String new_rut, new_codigo, new_nombre, new_apellido, new_sucursal;
    int tipo_add;

    private getData getData;
    private getAdd getAdd;
    private getSave getSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit);

        Bundle bundle = getIntent().getExtras();
        user_id = bundle.getInt(MyActivity.USER_ID);
        taller_id = bundle.getString(MyActivity.WORKSHOP_ID);
        tipo = bundle.getString(MyActivity.TYPE);

        TextView tv_user_id = (TextView) findViewById(R.id.tv_user_id);
        Button btn_add = (Button) findViewById(R.id.btn_add_asistente);
        Button btn_update = (Button) findViewById(R.id.btn_update_asistente);

        if (tipo.equals("1")) {
            setTitle("Nuevo Asistente");
            tv_user_id.setText("Ingrese datos del nuevo asistente");
            btn_add.setVisibility(View.VISIBLE);

            btn_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean valid = true;
                    EditText txt_rut = (EditText) findViewById(R.id.txtRutAsistente);
                    new_rut = txt_rut.getText().toString();
                    EditText txt_cod = (EditText) findViewById(R.id.txtCodAsistente);
                    new_codigo = txt_cod.getText().toString();
                    EditText txt_nombre = (EditText) findViewById(R.id.txtNombreAsistente);
                    new_nombre = txt_nombre.getText().toString();
                    EditText txt_apellido = (EditText) findViewById(R.id.txtApellidosAsistente);
                    new_apellido = txt_apellido.getText().toString();
                    TextView txt_sucursal = (TextView) findViewById(R.id.txtSucursalAsistente);
                    new_sucursal = txt_sucursal.getText().toString();
                    if (new_rut.equals("")) {
                        valid = false;
                    }
                    if (new_codigo.equals("")) {
                        valid = false;
                    }
                    if (new_nombre.equals("")) {
                        valid = false;
                    }
                    if (new_apellido.equals("")) {
                        valid = false;
                    }

                    if (valid) {
                        tipo_add = 1;
                        getAdd = new getAdd();
                        getAdd.execute();
                    } else {
                        Toast toast =
                                Toast.makeText(getApplicationContext(),
                                        "Debe completar todos los campos para guardar.", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
            });
        } else {
            setTitle("Editar Asistente");
            tv_user_id.setText("Datos del asistente seleccionado");
            btn_update.setVisibility(View.VISIBLE);
            btn_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean valid = true;
                    EditText txt_rut = (EditText) findViewById(R.id.txtRutAsistente);
                    new_rut = txt_rut.getText().toString();
                    EditText txt_cod = (EditText) findViewById(R.id.txtCodAsistente);
                    new_codigo = txt_cod.getText().toString();
                    EditText txt_nombre = (EditText) findViewById(R.id.txtNombreAsistente);
                    new_nombre = txt_nombre.getText().toString();
                    EditText txt_apellido = (EditText) findViewById(R.id.txtApellidosAsistente);
                    new_apellido = txt_apellido.getText().toString();
                    TextView txt_sucursal = (TextView) findViewById(R.id.txtSucursalAsistente);
                    new_sucursal = txt_sucursal.getText().toString();
                    if (new_rut.equals("")) {
                        valid = false;
                    }
                    if (new_codigo.equals("")) {
                        valid = false;
                    }
                    if (new_nombre.equals("")) {
                        valid = false;
                    }
                    if (new_apellido.equals("")) {
                        valid = false;
                    }

                    if (valid) {
                        getSave = new getSave();
                        getSave.execute();
                    } else {
                        Toast toast =
                                Toast.makeText(getApplicationContext(),
                                        "Debe completar todos los campos para guardar.", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
            });
        }
        getData = new getData();
        getData.execute(); //OBTENGO DATA DE WS
    }

    private class getData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                if (tipo.equals("1")) {
                    Log.d("URL ", URL_WS + "/get_data_taller.php?taller_id=" + taller_id);
                    url = new URL(URL_WS + "/get_data_taller.php?taller_id=" + taller_id);
                } else {
                    Log.d("URL ", URL_WS + "/get_data_inscrito.php?inscrito_id=" + user_id + "&action=data");
                    url = new URL(URL_WS + "/get_data_inscrito.php?inscrito_id=" + user_id + "&action=data");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {

            if (tipo.equals("1")) {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("taller");

                JSONObject row = dataArray.getJSONObject(0);

                TextView txt_sucursal = (TextView) findViewById(R.id.txtSucursalAsistente);
                txt_sucursal.setText(row.getString("sucursal"));

            } else {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("inscrito");

                JSONObject row = dataArray.getJSONObject(0);

                EditText txt_rut = (EditText) findViewById(R.id.txtRutAsistente);
                rut = row.getString("rut");
                txt_rut.setText(rut);
                EditText txt_cod = (EditText) findViewById(R.id.txtCodAsistente);
                codigo = row.getString("codigo");
                txt_cod.setText(codigo);
                EditText txt_nombre = (EditText) findViewById(R.id.txtNombreAsistente);
                nombre = row.getString("nombre");
                txt_nombre.setText(nombre);
                EditText txt_apellido = (EditText) findViewById(R.id.txtApellidosAsistente);
                apellido = row.getString("apellido");
                txt_apellido.setText(apellido);
                TextView txt_sucursal = (TextView) findViewById(R.id.txtSucursalAsistente);
                sucursal = row.getString("sucursal");
                txt_sucursal.setText(sucursal);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class getSave extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                String name = new_nombre.replace(" ", "_");
                String last_name = new_apellido.replace(" ", "_");

                Log.d("URL ", URL_WS + "/get_data_inscrito.php?nscrito_id=" + user_id + "&rut=" + new_rut + "&codigo=" + new_codigo + "&nombre=" + name + "&apellido=" + last_name + "&action=edit");
                url = new URL(URL_WS + "/get_data_inscrito.php?nscrito_id=" + user_id + "&rut=" + new_rut + "&codigo=" + new_codigo + "&nombre=" + name + "&apellido=" + last_name + "&action=edit");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultSave(result);
        }
    }

    public void getResultSave(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("inscrito");
            JSONObject row = dataArray.getJSONObject(0);
            int val = row.getInt("ok");
            if (val == 1) {
                Toast toast =
                        Toast.makeText(getApplicationContext(),
                                "Los datos se han guardado correctamente.", Toast.LENGTH_SHORT);
                toast.show();
            } else {
                Toast toast =
                        Toast.makeText(getApplicationContext(),
                                "A ocurrido un error intentelo más tarde.", Toast.LENGTH_SHORT);
                toast.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class getAdd extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                if (tipo_add == 1) {
                    Log.d("URL ", URL_WS + "/get_data_inscrito.php?rut=" + new_rut + "&codigo=" + new_codigo + "&action=validar");
                    url = new URL(URL_WS + "/get_data_inscrito.php?rut=" + new_rut + "&codigo=" + new_codigo + "&action=validar");
                } else {
                    String name = new_nombre.replace(" ", "_");
                    String last_name = new_apellido.replace(" ", "_");
                    String subsidiary = new_sucursal.replace(" ", "_");
                    Log.d("URL ", URL_WS + "/get_data_inscrito.php?rut=" + new_rut + "&codigo=" + new_codigo + "&nombre=" + name + "&apellido=" + last_name + "&sucursal=" + subsidiary + "&id_taller=" + taller_id + "&action=new");
                    url = new URL(URL_WS + "/get_data_inscrito.php?rut=" + new_rut + "&codigo=" + new_codigo + "&nombre=" + name + "&apellido=" + last_name + "&sucursal=" + subsidiary + "&id_taller=" + taller_id + "&action=new");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResultsAdd(result);
        }
    }

    public void getResultsAdd(String response) {
        JSONObject obj = null;
        try {

            if (tipo_add == 1) {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("inscrito");

                JSONObject row = dataArray.getJSONObject(0);

                int val = row.getInt("ok");

                if (val == 1) {
                    tipo_add = 2;

                    getAdd = new getAdd();
                    getAdd.execute();
                } else {
                    Toast toast =
                            Toast.makeText(getApplicationContext(),
                                    "El rut ingresado no es válido.", Toast.LENGTH_SHORT);
                    toast.show();
                }
            } else {
                //RESPUESTA DE URL
                obj = new JSONObject(response.toString());
                //OBTENGO EL ARRAY DENTRO DEL OBJETO
                JSONArray dataArray = obj.getJSONArray("inscrito");

                JSONObject row = dataArray.getJSONObject(0);

                int n = row.getInt("ok");

                if (n == 1) {
                    Toast toast =
                            Toast.makeText(getApplicationContext(),
                                    "Se agrego correctamente.", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    Toast toast =
                            Toast.makeText(getApplicationContext(),
                                    "A ocurrido un error intentelo más tarde.", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
