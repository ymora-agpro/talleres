package cl.agpro.talleres;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by franciscoriveros on 02-05-16.
 */
public class ArrayAdapterSpinner extends ArrayAdapter<ArraySpinner> {
    Context context;
    int layoutResourceId;
    ArraySpinners data = null;

    public ArrayAdapterSpinner(Context context, ArraySpinners data) {
        super(context, R.layout.spinner_item, data);

        this.context = context;
        layoutResourceId = R.layout.spinner_item;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        FollowData follow = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            follow = new FollowData();
            follow.txtTitle = (TextView) row.findViewById(R.id.spinner_id);
            row.setTag(follow);
        } else {
            follow = (FollowData) row.getTag();
        }


        final ArraySpinner followIndex = data.get(position);
        follow.txtTitle.setText(followIndex.title);
        final FollowData finalFollow = follow;


        return row;
    }

    static class FollowData {
        TextView txtTitle;
    }
}
