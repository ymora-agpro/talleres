package cl.agpro.talleres;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by yerkomora on 20-04-16.
 */
public class MyActivity extends AppCompatActivity {

    public static final String USER_ID = "USER_ID", WORKSHOP_ID = "WORKSHOP_ID", TYPE = "TYPE";

    public static final int REQUEST_CHALKBOARD = 0, REQUEST_GALLERY = 3, REQUEST_ACCOUNT = 6, REQUEST_RECORDS = 9;

    public static final int REQUEST_CHALKBOARD_CAMERA = 1, REQUEST_CHALKBOARD_GALLERY = 2;
    public static final int REQUEST_GALLERY_CAMERA = 4, REQUEST_GALLERY_GALLERY = 5;
    public static final int REQUEST_ACCOUNT_CAMERA = 7, REQUEST_ACCOUNT_GALLERY = 8;
    public static final int REQUEST_RECORDS_CAMERA = 10, REQUEST_RECORDS_GALLERY = 11;

    //URL FOR WEBSERVICE
    public static String URL_WS = "http://aequus.cl/ws";

    public String mCurrentPhotoPath = "";
    public Bitmap bitmap = null;
    public File imageCamera;

    public void goToWelcome() {
        Intent intent = new Intent(getBaseContext(), WelcomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }

    public void closeSession() {
        SharedPreferences prefs = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
        goToWelcome();
    }

    public void goToLogin() {
        Intent intent = new Intent(getBaseContext(), LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }

    // Go To Home
    public void goToHome() {
        Intent intent = new Intent(getBaseContext(), HomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }

    public void goToHomeProfe() {
        Intent intent = new Intent(getBaseContext(), HomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
        finish();
    }

    public void goToAssistance(String workshop_id) {
        Intent intent = new Intent(getBaseContext(), AssistanceActivity.class);
        intent.putExtra(WORKSHOP_ID, workshop_id);
        startActivity(intent);
    }

    public void goToMedicalRecords(String workshop_id) {
        Intent intent = new Intent(getBaseContext(), MedicalRecordsActivity.class);
        intent.putExtra(WORKSHOP_ID, workshop_id);
        startActivity(intent);
    }

    public void goToUserEdit(int user_id, String taller_id, String tipo) {
        Intent intent = new Intent(getBaseContext(), UserEditActivity.class);
        intent.putExtra(USER_ID, user_id);
        intent.putExtra(WORKSHOP_ID, taller_id);
        intent.putExtra(TYPE, tipo);
        startActivity(intent);
    }

    public void goToRegistered(String workshop_id) {
        Intent intent = new Intent(getBaseContext(), RegisteredActivity.class);
        intent.putExtra(WORKSHOP_ID, workshop_id);
        startActivity(intent);
    }

    public void hide(View view) {
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    public void questionExit() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getText(R.string.exit_title))
                .setMessage(getText(R.string.exit_question))
                .setPositiveButton(getText(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(getText(R.string.not), null)
                .show();
    }

    public void usuarioNoExiste() {
        Toast toast =
                Toast.makeText(getApplicationContext(),
                        R.string.usuario_no_existe, Toast.LENGTH_SHORT);

        toast.show();
    }

    public void uploadPhoto(final Activity activity, final int REQUEST) {
        Intent camIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        Intent gallIntent = new Intent(Intent.ACTION_GET_CONTENT);
        gallIntent.setType("image/*");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, android.R.layout.select_dialog_item);

        // look for available intents
        //List<ResolveInfo> info = new ArrayList<ResolveInfo>();
        List<Intent> yourIntentsList = new ArrayList<Intent>();

        PackageManager packageManager = activity.getPackageManager();

        List<ResolveInfo> listCam = packageManager.queryIntentActivities(camIntent, 0);
        for (ResolveInfo res : listCam) {
            final Intent finalIntent = new Intent(camIntent);
            finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            yourIntentsList.add(finalIntent);
            //info.add(res);

            adapter.add(res.loadLabel(packageManager).toString());
        }

        List<ResolveInfo> listGall = packageManager.queryIntentActivities(gallIntent, 0);
        for (ResolveInfo res : listGall) {
            final Intent finalIntent = new Intent(gallIntent);
            finalIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            yourIntentsList.add(finalIntent);
            //info.add(res);

            adapter.add(res.loadLabel(packageManager).toString());
        }


        final List<Intent> yourIntentsListFinal = yourIntentsList;

        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);

        dialog.setTitle(getBaseContext().getResources().getString(R.string.media_capture_title));
        dialog.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                Intent intent = yourIntentsListFinal.get(item);

                if (intent.getAction().equals("android.media.action.IMAGE_CAPTURE"))
                    dispatchTakePictureIntent(REQUEST + 1);
                else
                    startActivityForResult(intent, REQUEST + 2);
            }
        });

        dialog.show();
    }

    // Camera

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        // content://media/external/images/media/31
        // /storage/sdcard/DCIM/Camera/IMG_20160420_145850.jpg

        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );


        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getPath();
        imageCamera = image;
        return image;
    }

    private void dispatchTakePictureIntent(final int REQUEST) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST);
            }
        }
    }

    // Images

    public static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(getCircularBitmap(result));
            bmImage.setVisibility(View.VISIBLE);
        }
    }

    public static Bitmap getCircularBitmap(Bitmap bitmap) {

        int radius = Math.min(bitmap.getWidth(), bitmap.getWidth());

        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap bmp = Bitmap.createBitmap(radius, radius, conf);
        Canvas canvas = new Canvas(bmp);

        // creates a centered bitmap of the desired size
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, radius, radius, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setAntiAlias(true);

        paint.setShader(shader);
        RectF rect = new RectF(0, 0, radius, radius);
        canvas.drawRoundRect(rect, radius, radius, paint);

        return bmp;
    }

    // Android Marshmallow Permission

    public boolean mayRequestCamera(View view, final int REQUEST) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            List<String> permission = new ArrayList<>();

            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED)
                permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED)
                permission.add(Manifest.permission.CAMERA);

            final String[] permissionArray = new String[permission.size()];
            permission.toArray(permissionArray);

            if (permission.size() > 0)
                requestPermissions(permissionArray, REQUEST + 1);
            else
                uploadPhoto(this, REQUEST);
        } else {
            uploadPhoto(this, REQUEST);
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        for (int grantResult : grantResults)
            if (grantResult == PackageManager.PERMISSION_DENIED)
                return;

        uploadPhoto(this, requestCode - 1);
    }

    // Scale Bitmap
    public Bitmap getBitmapScaleWidth(Bitmap bitmap, int width) {
        int height = width * bitmap.getHeight() / bitmap.getWidth();
        return Bitmap.createScaledBitmap(bitmap, width, height, false);
    }

    // Memory
    public void getMaxMemory() {
        // Samsung Galaxy Grand Prime
        // maxMemory: 268.435.456

        // Moto G
        // maxMemory: 268.435.456

        // Sony Xperia Z5
        // maxMemory: 536.870.912

        Runtime rt = Runtime.getRuntime();
        long maxMemory = rt.maxMemory();
        Log.v("onCreate", "maxMemory:" + Long.toString(maxMemory));
    }

    public void getMemoryClass() {
        // Samsung Galaxy Grand Prime
        // memoryClass: 96

        // Moto G
        // memoryClass: 96

        // Sony Xperia Z5
        // memoryClass: 192

        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        int memoryClass = am.getMemoryClass();
        Log.v("onCreate", "memoryClass:" + Integer.toString(memoryClass));
    }
}
