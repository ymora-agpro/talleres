package cl.agpro.talleres.photo;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhotoContent {

    public static final List<PhotoItem> ITEMS = new ArrayList<>();

    public static final Map<String, PhotoItem> ITEM_MAP = new HashMap<>();

    public static void addItem(PhotoItem item) {
        ITEMS.add(0, item);
        ITEM_MAP.put(item.id, item);
    }

    public static class PhotoItem {
        public final String id;
        public final Bitmap photo;

        public PhotoItem(String id, Bitmap photo) {
            this.id = id;
            this.photo = photo;
        }

        @Override
        public String toString() {
            return id;
        }
    }
}
