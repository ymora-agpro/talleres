package cl.agpro.talleres;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WorkshopsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WorkshopsFragment extends Fragment {


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private GetData getData;
    private GetData2 getData2;

    Button cargar, subir;

    Spinner spinSucursal, spinTaller;
    String strinSucursal, strinTaller;
    String type;
    int user_id;

    private ArraySpinners spinner, spinners;
    private ArrayAdapterSpinner adapterDynamic;

    private Context context;
    private HomeActivity homeActivity;

    private static final String TAG = "WorkshopsFragment";

    public void TallerFragment() {
        // Required empty public constructor
    }

    public void setActivity(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TallerFragmen.
     */
    // TODO: Rename and change types and number of parameters
    public static WorkshopsFragment newInstance(String param1, String param2) {
        WorkshopsFragment fragment = new WorkshopsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        SharedPreferences prefs = context.getSharedPreferences("usuario", Context.MODE_PRIVATE);
        type = prefs.getString("type", "");
        user_id = prefs.getInt("id", 0);

        Log.d(TAG, String.valueOf(user_id));

        getData = new GetData();
        getData.execute(); //OBTENGO DATA DE WS

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_workshops, container, false);

        // Spinner Workshops
        final Spinner s_workshops = (Spinner) view.findViewById(R.id.s_workshops);

        // Spinner Subsidiaries
        spinSucursal = (Spinner) view.findViewById(R.id.s_subsidiaries);

        spinTaller = (Spinner) view.findViewById(R.id.s_workshops);

        // Button
        cargar = (Button) view.findViewById(R.id.cargar_asistencia);
        subir = (Button) view.findViewById(R.id.subir_fichas);

        if (type.equals("caja"))
            subir.setVisibility(View.VISIBLE);
        else
            cargar.setVisibility(View.VISIBLE);

        cargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] nombre_taller = strinTaller.split(" - ");
                String taller_id = nombre_taller[1];
                Log.d(TAG, taller_id);

                homeActivity.goToAssistance(taller_id);
            }
        });

        subir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] nombre_taller = strinTaller.split(" - ");
                String taller_id = nombre_taller[1];
                Log.d(TAG, taller_id);

                homeActivity.goToMedicalRecords(taller_id);
            }
        });

        return view;
    }

    private class GetData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            String sURL;
            try {

                if (type.equals("admin")) {
                    sURL = homeActivity.URL_WS + "/get_list_sucursales.php";
                } else {
                    sURL = homeActivity.URL_WS + "/get_list_sucursales.php?action=caja&user_id=" + user_id;
                }
                Log.d(TAG, sURL);
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);

            if (spinSucursal != null) {
                spinSucursal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        ArraySpinner arraySpinner = (ArraySpinner) parent.getItemAtPosition(position);
                        //Toast.makeText(ActivityArriendo.this, arriendoTipo.toString(), Toast.LENGTH_LONG).show();

                        strinSucursal = arraySpinner.toString();

                        getData2 = new GetData2();
                        getData2.execute(); //OBTENGO DATA DE WS
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("sucursales");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            spinner = new ArraySpinners();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                int id = 0;
                String nombre = row.getString("nombre");
                spinner.add(new ArraySpinner(nombre, id));
            }

            spinners = new ArraySpinners();
            spinners.addAll(spinner);

            adapterDynamic = new ArrayAdapterSpinner(getActivity(), spinners);
            spinSucursal.setAdapter(adapterDynamic);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class GetData2 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                String stringSuc = strinSucursal.replace(" ", "_");
                String sURL = homeActivity.URL_WS + "/get_list_talleres.php?sucursal_id=" + stringSuc;
                Log.d(TAG, sURL);
                Log.d("QUE HAY AQUI: ", sURL);
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults2(result);

            if (spinTaller != null) {
                spinTaller.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        ArraySpinner arraySpinner = (ArraySpinner) parent.getItemAtPosition(position);
                        //Toast.makeText(ActivityArriendo.this, arriendoTipo.toString(), Toast.LENGTH_LONG).show();

                        strinTaller = arraySpinner.toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    public void getResults2(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("talleres");

            //CREO EL ARRAY QUE CONTIENE MI CLASE DE FOLLOW
            spinner = new ArraySpinners();

            //RELLENO MI CLASE CON LA INFORMACION DEL WS
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject row = dataArray.getJSONObject(i);
                int id = 0;
                String nombre = row.getString("nombre");
                spinner.add(new ArraySpinner(nombre, id));
            }

            spinners = new ArraySpinners();
            spinners.addAll(spinner);

            adapterDynamic = new ArrayAdapterSpinner(getActivity(), spinners);
            spinTaller.setAdapter(adapterDynamic);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
