package cl.agpro.talleres;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import cl.agpro.talleres.PersonFragment.OnListFragmentInteractionListener;
import cl.agpro.talleres.person.Person.PersonItem;
import cl.agpro.talleres.webservice.MultipartUtility;
import cl.agpro.talleres.webservice.MyPost;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link PersonItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyPersonRecyclerViewAdapter extends RecyclerView.Adapter<MyPersonRecyclerViewAdapter.ViewHolder> {

    private final List<PersonItem> mValues;
    private final OnListFragmentInteractionListener mListener;
    private final String mtipo, mWorkShopId;

    public MyPersonRecyclerViewAdapter(List<PersonItem> items, OnListFragmentInteractionListener listener, String tipo, String workshop_id) {
        mValues = items;
        mListener = listener;
        mtipo = tipo;
        mWorkShopId = workshop_id;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (mtipo.equals("1")) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_person, parent, false);
        } else if (mtipo.equals("2")) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_person_register, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_person_registered, parent, false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNameView.setText(mValues.get(position).name);
        holder.mRutView.setText(mValues.get(position).rut);

        switch (mtipo) {
            case "1":
                SharedPreferences prefs = holder.mView.getContext().getSharedPreferences("usuario", Context.MODE_PRIVATE);
                String type = prefs.getString("type", "");

                if (type.equals("admin")) {
                    holder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mListener != null) {

                                Intent intent = new Intent(v.getContext(), UserEditActivity.class);
                                intent.putExtra(MyActivity.USER_ID, holder.mItem.id);
                                intent.putExtra(MyActivity.TYPE, "2");
                                v.getContext().startActivity(intent);

                                // Notify the active callbacks interface (the activity, if the
                                // fragment is attached to one) that an item has been selected.
                                mListener.onListFragmentInteraction(holder.mItem);
                            }
                        }
                    });
                }

                holder.mcheckBox.setChecked(mValues.get(position).assist);

                //holder.mItem.assist = holder.mcheckBox.isChecked();

                holder.mcheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        holder.mItem.assist = isChecked;
                    }
                });
                break;
            case "2":
                if (mValues.get(position).inscrito.equals("1"))
                    holder.mcheckBox.setChecked(true);

                holder.mcheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        holder.mItem.assist = isChecked;
                        new AsyncTaskRegister(mWorkShopId, holder.mItem.id.toString()).execute();
                    }
                });
                break;
            default:
                if (mValues.get(position).ficha.equals("1")) {
                    holder.mFichaView.setImageResource(R.mipmap.ic_heart_36dp);
                } else {
                    holder.mFichaView.setImageResource(R.mipmap.ic_unheart_36dp);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final TextView mRutView;
        public final ImageView mFichaView;
        public final CheckBox mcheckBox;

        public PersonItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = (TextView) view.findViewById(R.id.name);
            mRutView = (TextView) view.findViewById(R.id.rut);
            mFichaView = (ImageView) view.findViewById(R.id.imgHeart);
            mcheckBox = (CheckBox) view.findViewById(R.id.checkBox);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mRutView.getText() + "'";
        }
    }

    private class AsyncTaskRegister extends AsyncTask<Void, Void, Boolean> {
        private static final String TAG = "AsyncTaskRegister";
        private final String URL = MyActivity.URL_WS + "/get_accion_inscrito.php";

        private final String workshop_id, register_id;

        public AsyncTaskRegister(String id_workshop, String id_register) {
            this.workshop_id = id_workshop;
            this.register_id = id_register;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            MultipartUtility multipart = null;
            try {
                // get_accion_inscrito.php?taller_id=1&id_insc=2
                multipart = new MultipartUtility(URL, "utf-8");

                // Post
                List<MyPost> myFormDataArray = new ArrayList<MyPost>();

                myFormDataArray.add(new MyPost("taller_id", workshop_id));
                myFormDataArray.add(new MyPost("id_insc", register_id));

                for (int i = 0; i < myFormDataArray.size(); i++) {
                    multipart.addFormField(myFormDataArray.get(i).getName(),
                            myFormDataArray.get(i).getValue());
                }

                String responseString;
                List<String> response = multipart.finish();
                Log.e(TAG, "SERVER REPLIED:");
                for (String line : response) {
                    Log.e(TAG, "Response:::" + line);
                    // get your server response here.
                    responseString = line;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
