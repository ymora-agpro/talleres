package cl.agpro.talleres;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cl.agpro.talleres.photo.PhotoContent;
import cl.agpro.talleres.webservice.MultipartUtility;
import cl.agpro.talleres.webservice.MyFile;
import cl.agpro.talleres.webservice.MyPost;

public class MedicalRecordsActivity extends MyActivity {

    private String workshop_id, user_id, gallery_id;

    private List<MyFile> myFileArray = new ArrayList<>();

    private static final String URL_MEDICAL_RECORDS = URL_WS + "/add_fichas.php";

    private PhotoContent photoContent;
    private RecyclerView recyclerView;
    MyPhotoRecyclerViewAdapter myPhotoRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_records);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        workshop_id = bundle.getString(MyActivity.WORKSHOP_ID);

        SharedPreferences prefs = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        user_id = String.valueOf(prefs.getInt("id", 0));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null)
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (myFileArray.size() < 10)
                        mayRequestCamera(v, MyActivity.REQUEST_RECORDS);
                    else
                        Toast.makeText(getBaseContext(), R.string.medical_records_maximum, Toast.LENGTH_SHORT).show();
                }
            });

        recyclerView = (RecyclerView) findViewById(R.id.list);
        if (recyclerView != null) {
            recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

            photoContent = new PhotoContent();
            myPhotoRecyclerViewAdapter = new MyPhotoRecyclerViewAdapter(photoContent.ITEMS, new PhotoFragment.OnListFragmentInteractionListener() {
                @Override
                public void onListFragmentInteraction(PhotoContent.PhotoItem item) {
                }
            });

            recyclerView.setAdapter(myPhotoRecyclerViewAdapter);
        }

        getMaxMemory();
        getMemoryClass();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_medical_records, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_send) {
            if (myFileArray.size() < 1)
                Toast.makeText(this, R.string.medical_records_required, Toast.LENGTH_SHORT).show();
            else {
                new AsyncTaskGallery().execute();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String name = "foto";
            File file = null;

            if (requestCode == REQUEST_RECORDS_GALLERY) {
                Uri imageUri = data.getData();
                file = new File(getRealPathFromUri(this, imageUri));

                Toast.makeText(this, getString(R.string.upload_image_success), Toast.LENGTH_SHORT).show();
            } else if (requestCode == REQUEST_RECORDS_CAMERA) {
                file = new File(mCurrentPhotoPath);
            }

            // File To Bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), bmOptions);

            // Scale
            bitmap = getBitmapScaleWidth(bitmap, 500);

            // RecyclerView
            addItem(new PhotoContent.PhotoItem("", bitmap));

            //Bitmap To File
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Server
            myFileArray.add(new MyFile(name, file));
        }
    }

    public void addItem(PhotoContent.PhotoItem item) {
        photoContent.addItem(item);
        recyclerView.getAdapter().notifyItemInserted(0);
        recyclerView.getAdapter().notifyItemRangeChanged(1, photoContent.ITEMS.size() - 1);

        recyclerView.smoothScrollToPosition(0);
    }

    public class AsyncTaskGallery extends AsyncTask {
        private static final String TAG = "atGallery";

        @Override
        protected Object doInBackground(Object[] params) {
            MultipartUtility multipart = null;
            try {
                // http://aequus.cl/ws/add_fichas.php?id_suc=1&type=caja&id_taller=1&action=gallery
                multipart = new MultipartUtility(URL_MEDICAL_RECORDS, "utf-8");

                // Post
                List<MyPost> myFormDataArray = new ArrayList<>();

                myFormDataArray.add(new MyPost("id_suc", user_id));
                myFormDataArray.add(new MyPost("type", "caja"));
                myFormDataArray.add(new MyPost("id_taller", workshop_id));
                myFormDataArray.add(new MyPost("action", "gallery"));

                myFormDataArray.add(new MyPost("ADD", "ADD"));
                myFormDataArray.add(new MyPost("ADD", "ADD"));
                myFormDataArray.add(new MyPost("ADD", "ADD"));

                for (int i = 0; i < myFormDataArray.size(); i++) {
                    multipart.addFormField(myFormDataArray.get(i).getName(),
                            myFormDataArray.get(i).getValue());
                }

                String responseString = "";
                List<String> response = multipart.finish();
                Log.e(TAG, "SERVER REPLIED:");
                for (String line : response) {
                    Log.e(TAG, "Upload Files Response:::" + line);
                    // get your server response here.
                    responseString = line;
                }

                // JSON
                try {
                    JSONObject obj = new JSONObject(responseString);

                    if (obj.getInt("success") == 1) {
                        JSONArray dataArray = obj.getJSONArray("results");
                        JSONObject row = dataArray.getJSONObject(0);
                        gallery_id = row.getString("gallery_id");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            new AsyncTaskGalleryPhotos().execute();
        }
    }

    public class AsyncTaskGalleryPhotos extends AsyncTask<Void, Void, Boolean> {
        private static final String TAG = "atGalleryPhotos";

        @Override
        protected Boolean doInBackground(Void... params) {
            MultipartUtility multipart = null;
            try {
                // http://aequus.cl/ws/add_fichas.php?galeria_id=1&action=fotos_galeria
                // $_FILES
                multipart = new MultipartUtility(URL_MEDICAL_RECORDS, "utf-8");

                // Post
                List<MyPost> myFormDataArray = new ArrayList<>();

                myFormDataArray.add(new MyPost("galeria_id", gallery_id));
                myFormDataArray.add(new MyPost("action", "fotos_galeria"));

                myFormDataArray.add(new MyPost("ADD", "ADD"));
                myFormDataArray.add(new MyPost("ADD", "ADD"));
                myFormDataArray.add(new MyPost("ADD", "ADD"));

                for (int i = 0; i < myFormDataArray.size(); i++) {
                    multipart.addFormField(myFormDataArray.get(i).getName(),
                            myFormDataArray.get(i).getValue());
                }

                // Files
                for (int i = 0; i < myFileArray.size(); i++) {
                    multipart.addFilePart(String.valueOf(i),
                            myFileArray.get(i).getValue());
                }

                String responseString = "";
                List<String> response = multipart.finish();
                Log.e(TAG, "SERVER REPLIED:");
                for (String line : response) {
                    Log.e(TAG, "Upload Files Response:::" + line);
                    // get your server response here.
                    responseString = line;
                }

                // JSON
                try {
                    JSONObject obj = new JSONObject(responseString);

                    if (obj.getInt("success") == 1) {
                        JSONArray dataArray = obj.getJSONArray("results");
                        JSONObject row = dataArray.getJSONObject(0);
                        return row.getInt("ok") == 1;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if (result) {
                Toast.makeText(MedicalRecordsActivity.this, R.string.medical_records_success, Toast.LENGTH_SHORT).show();

                // Clear
                photoContent.ITEMS.clear();
                myPhotoRecyclerViewAdapter.notifyDataSetChanged();

                myFileArray.clear();
            }
        }
    }
}
