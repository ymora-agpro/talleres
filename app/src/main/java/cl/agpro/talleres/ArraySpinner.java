package cl.agpro.talleres;

/**
 * Created by franciscoriveros on 02-05-16.
 */
public class ArraySpinner {
    public String title;
    public int id;

    public ArraySpinner(String title, int id) {
        this.title = title;
        this.id = id;
    }

    @Override
    public String toString() {
        return title;
    }
}
