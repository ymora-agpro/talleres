package cl.agpro.talleres;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cl.agpro.talleres.webservice.MultipartUtility;
import cl.agpro.talleres.webservice.MyPost;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final String TAG = "AccountFragment";
    String type, image, user_id;

    String name, email, phone, new_phone;

    private Context context;
    private HomeActivity homeActivity;
    private MyActivity myActivity;

    private GetData getData;
    private GetData2 getData2;
    private View rootView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ImageView imgUs;

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.homeActivity = (HomeActivity) context;
        this.myActivity = (MyActivity) context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AccountFragment newInstance(String param1, String param2) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        SharedPreferences prefs = context.getSharedPreferences("usuario", Context.MODE_PRIVATE);
        type = prefs.getString("type", "");
        user_id = String.valueOf(prefs.getInt("id", 0));
        image = prefs.getString("image", "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_account, container, false);

        imgUs = (ImageView) rootView.findViewById(R.id.imgUs);

        if (image.isEmpty())
            imgUs.setVisibility(View.VISIBLE);
        else
            new MyActivity.DownloadImageTask(imgUs)
                    .execute(image);

        ImageButton btnCamera = (ImageButton) rootView.findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myActivity.mayRequestCamera(v, MyActivity.REQUEST_ACCOUNT);
                //myActivity.uploadPhoto(myActivity, MyActivity.REQUEST_ACCOUNT);
            }
        });


        if (type.equals("admin")) {
            TextView lbl_phone = (TextView) rootView.findViewById(R.id.lblPhoneUser);
            lbl_phone.setVisibility(View.GONE);

            EditText txt_phone = (EditText) rootView.findViewById(R.id.txtPhoneUser);
            txt_phone.setVisibility(View.GONE);
        }

        Button save = (Button) rootView.findViewById(R.id.btn_save_data);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("profe")) {
                    EditText txt_name = (EditText) rootView.findViewById(R.id.txtNameUser);
                    name = txt_name.getText().toString();

                    EditText txt_email = (EditText) rootView.findViewById(R.id.txtEmailUser);
                    email = txt_email.getText().toString();

                    EditText txt_phone = (EditText) rootView.findViewById(R.id.txtPhoneUser);
                    new_phone = txt_phone.getText().toString();
                    Log.d(TAG, new_phone);

                    getData2 = new GetData2();
                    getData2.execute(); //OBTENGO DATA DE WS
                }

                new AsyncTaskPhotoAccount().execute();
            }
        });

        getData = new GetData();
        getData.execute(); //OBTENGO DATA DE WS

        return rootView;
    }

    private class GetData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            String sURL;
            try {

                sURL = homeActivity.URL_WS + "/get_data_user.php?user_id=" + user_id + "&type=" + type;
                Log.d(TAG, sURL);
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("user");

            JSONObject row = dataArray.getJSONObject(0);

            TextView txt_name = (TextView) rootView.findViewById(R.id.txtNameUser);
            txt_name.setText(row.getString("nombre"));
            TextView txt_email = (TextView) rootView.findViewById(R.id.txtEmailUser);
            txt_email.setText(row.getString("correo"));
            TextView txt_phone = (TextView) rootView.findViewById(R.id.txtPhoneUser);
            txt_phone.setText(row.getString("telefono"));
            phone = row.getString("telefono");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class GetData2 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            String sURL;
            try {

                sURL = homeActivity.URL_WS + "/get_data_user.php?user_id=" + user_id + "&type=edit_" + type + "&nombre=&correo=&telefono=" + new_phone;
                Log.d(TAG, sURL);
                url = new URL(sURL);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults2(result);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public void getResults2(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("user");

            JSONObject row = dataArray.getJSONObject(0);

            int success = row.getInt("ok");
            if (success == 1) {
                Toast toast =
                        Toast.makeText(context,
                                R.string.msg_save, Toast.LENGTH_SHORT);
                toast.show();
            } else {
                EditText txt_phone = (EditText) rootView.findViewById(R.id.txtPhoneUser);
                txt_phone.setText(phone);
                Toast toast =
                        Toast.makeText(context,
                                R.string.msg_error_save, Toast.LENGTH_SHORT);
                toast.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setPhoto(Bitmap bitmap) {
        imgUs.setImageBitmap(bitmap);
    }

    public class AsyncTaskPhotoAccount extends AsyncTask {
        private static final String TAG = "atPhotoAccount";
        private final String URL_PHOTO_ACCOUNT = MyActivity.URL_WS + "/upload_image_profile.php";

        @Override
        protected Object doInBackground(Object[] params) {
            MultipartUtility multipart = null;
            try {
                // ?action=foto_pizarra&galeria_id=1
                // $_FILES[foto]
                multipart = new MultipartUtility(URL_PHOTO_ACCOUNT, "utf-8");

                // Post
                List<MyPost> myFormDataArray = new ArrayList<MyPost>();

                myFormDataArray.add(new MyPost("user_id", user_id));
                myFormDataArray.add(new MyPost("type", type));

                for (int i = 0; i < myFormDataArray.size(); i++) {
                    multipart.addFormField(myFormDataArray.get(i).getName(),
                            myFormDataArray.get(i).getValue());
                }


                if (homeActivity.myFile != null)
                    multipart.addFilePart(homeActivity.myFile.getName(),
                            homeActivity.myFile.getValue());

                String responseString = "";
                List<String> response = multipart.finish();
                Log.e(TAG, "SERVER REPLIED:");
                for (String line : response) {
                    Log.e(TAG, "Upload Files Response:::" + line);
                    // get your server response here.
                    responseString = line;
                }

                // JSON
                try {
                    // {"results":[{"ok":1}],"success":1}
                    JSONObject obj = new JSONObject(responseString);

                    if (obj.getInt("success") == 1) {
                        JSONArray dataArray = obj.getJSONArray("results");
                        JSONObject row = dataArray.getJSONObject(0);
                        row.getString("ok");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            SharedPreferences prefs = homeActivity.getSharedPreferences("usuario", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("image", "");
            editor.commit();

            image = "";
        }
    }
}
