package cl.agpro.talleres;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cl.agpro.talleres.photo.PhotoContent;
import cl.agpro.talleres.person.PersonContent;
import cl.agpro.talleres.webservice.MultipartUtility;
import cl.agpro.talleres.webservice.MyFile;
import cl.agpro.talleres.webservice.MyPost;

public class AssistanceActivity extends MyActivity
        implements PersonFragment.OnListFragmentInteractionListener
        , AssistanceDetailsFragment.OnFragmentInteractionListener
        , PhotoFragment.OnListFragmentInteractionListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private String workshop_id, workshop_date, gallery_id;
    private FloatingActionButton fab;

    private AssistanceDetailsFragment assistanceDetailsFragment;
    private PhotoFragment photoFragment;
    private PersonFragment personFragment;

    private List<MyFile> myFileArray = new ArrayList<>();
    private MyFile myFile = null;

    private static final String URL_ASSISTANCE = URL_WS + "/add_asistencia.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assistance);

        Bundle bundle = getIntent().getExtras();
        workshop_id = bundle.getString(MyActivity.WORKSHOP_ID);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                if (fab.getVisibility() == View.VISIBLE)
                    fab.setVisibility(View.INVISIBLE);

                switch (position) {
                    case 1:
                        SharedPreferences prefs = getSharedPreferences("usuario", Context.MODE_PRIVATE);
                        String type = prefs.getString("type", "");

                        if (type.equals("admin")) {
                            fab.setImageResource(R.mipmap.ic_dialog_registered_36dp);
                            fab.show();
                        }

                        break;
                    case 2:
                        fab.setImageResource(R.mipmap.ic_dialog_gallery_36dp);
                        fab.show();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (tabLayout.getSelectedTabPosition()) {
                    case 1:
                        goToRegistered(workshop_id);
                        break;
                    case 2:
                        if (myFileArray.size() < 10)
                            mayRequestCamera(v, MyActivity.REQUEST_GALLERY);
                        else
                            Toast.makeText(getBaseContext(), R.string.assistance_gallery_maximum, Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_assistance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_send) {

            String txt_date = assistanceDetailsFragment.getDate();

            if (txt_date == null || txt_date.isEmpty())
                Toast.makeText(this, R.string.assistance_date_required, Toast.LENGTH_SHORT).show();
            else {
                workshop_date = txt_date;
                new AsyncTaskAssistance().execute();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String name = "foto";
            File file = null;

            if (requestCode == REQUEST_GALLERY_GALLERY || requestCode == REQUEST_CHALKBOARD_GALLERY) {
                //Uri imageUri = data.getData();
                file = new File(getRealPathFromUri(this, data.getData()));
            } else if (requestCode == REQUEST_GALLERY_CAMERA || requestCode == REQUEST_CHALKBOARD_CAMERA) {
                file = new File(mCurrentPhotoPath);
                //Uri myUri = Uri.parse(mCurrentPhotoPath);
            }

            // File To Bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), bmOptions);

            // Back
            this.bitmap = bitmap;

            // View
            if (requestCode == REQUEST_GALLERY_GALLERY || requestCode == REQUEST_GALLERY_CAMERA) {
                bitmap = getBitmapScaleWidth(bitmap, 500);
                photoFragment.addItem(new PhotoContent.PhotoItem("", bitmap));
            } else if (requestCode == REQUEST_CHALKBOARD_GALLERY || requestCode == REQUEST_CHALKBOARD_CAMERA) {
                bitmap = getBitmapScaleWidth(bitmap, 1000);
                assistanceDetailsFragment.setPhoto(bitmap);
            }

            //Bitmap To File
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Server
            if (requestCode == REQUEST_GALLERY_GALLERY || requestCode == REQUEST_GALLERY_CAMERA)
                myFileArray.add(new MyFile(name, file));
            else if (requestCode == REQUEST_CHALKBOARD_GALLERY || requestCode == REQUEST_CHALKBOARD_CAMERA)
                myFile = new MyFile(name, file);

            //Toast.makeText(this, getString(R.string.upload_image_success), Toast.LENGTH_SHORT).show();
        }
    }

    // AsyncTask

    public class AsyncTaskAssistance extends AsyncTask {
        private static final String TAG = "atAssistance";

        @Override
        protected Object doInBackground(Object[] params) {
            MultipartUtility multipart = null;
            try {
                // ?action=asistencias&id_taller=1&f_ini=2016-05-13
                // &inscrito=10&asiste=1
                // &obs=texto
                multipart = new MultipartUtility(URL_ASSISTANCE, "utf-8");

                // Post
                List<MyPost> myFormDataArray = new ArrayList<>();

                myFormDataArray.add(new MyPost("action", "asistencias"));
                myFormDataArray.add(new MyPost("id_taller", workshop_id));
                myFormDataArray.add(new MyPost("f_ini", workshop_date));

                List<PersonContent.PersonItem> persons = personFragment.getPersons();

                for (PersonContent.PersonItem person : persons) {
                    myFormDataArray.add(new MyPost("asistentes[" + person.id + "][inscrito]", person.id.toString()));
                    myFormDataArray.add(new MyPost("asistentes[" + person.id + "][asiste]", person.assist ? "1" : "0"));
                }

                myFormDataArray.add(new MyPost("obs", assistanceDetailsFragment.getObservation()));

                for (int i = 0; i < myFormDataArray.size(); i++) {
                    multipart.addFormField(myFormDataArray.get(i).getName(),
                            myFormDataArray.get(i).getValue());
                }

                String responseString;
                List<String> response = multipart.finish();
                Log.e(TAG, "SERVER REPLIED:");
                for (String line : response) {
                    Log.e(TAG, "Upload Files Response:::" + line);
                    // get your server response here.
                    responseString = line;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            new AsyncTaskGallery().execute();

            new AsyncTaskEmail().execute();
        }
    }

    public class AsyncTaskGallery extends AsyncTask {
        private static final String TAG = "atGallery";

        @Override
        protected Object doInBackground(Object[] params) {
            MultipartUtility multipart = null;
            try {
                // ?action=gallery&id_taller=1&f_ini=2016-05-10

                multipart = new MultipartUtility(URL_ASSISTANCE, "utf-8");

                // Post
                List<MyPost> myFormDataArray = new ArrayList<MyPost>();

                myFormDataArray.add(new MyPost("action", "gallery"));
                myFormDataArray.add(new MyPost("id_taller", workshop_id));
                myFormDataArray.add(new MyPost("f_ini", workshop_date));

                for (int i = 0; i < myFormDataArray.size(); i++) {
                    multipart.addFormField(myFormDataArray.get(i).getName(),
                            myFormDataArray.get(i).getValue());
                }

                String responseString = "";
                List<String> response = multipart.finish();
                Log.e(TAG, "SERVER REPLIED:");
                for (String line : response) {
                    Log.e(TAG, "Upload Files Response:::" + line);
                    // get your server response here.
                    responseString = line;
                }

                // JSON
                try {
                    // {"results":[{"gallery_id":74}],"success":1}
                    JSONObject obj = new JSONObject(responseString);

                    if (obj.getInt("success") == 1) {
                        JSONArray dataArray = obj.getJSONArray("results");
                        JSONObject row = dataArray.getJSONObject(0);
                        gallery_id = row.getString("gallery_id");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            new AsyncTaskGalleryPhotos().execute();
            new AsyncTaskPhotoChalkBoard().execute();
        }
    }

    public class AsyncTaskGalleryPhotos extends AsyncTask<Void, Void, Boolean> {
        private static final String TAG = "atGalleryPhotos";

        @Override
        protected Boolean doInBackground(Void... params) {
            MultipartUtility multipart = null;
            try {
                // ?action=fotos_galeria&galeria_id=1
                // $_FILES
                multipart = new MultipartUtility(URL_ASSISTANCE, "utf-8");

                // Post
                List<MyPost> myFormDataArray = new ArrayList<MyPost>();

                myFormDataArray.add(new MyPost("action", "fotos_galeria"));
                myFormDataArray.add(new MyPost("galeria_id", gallery_id));

                for (int i = 0; i < myFormDataArray.size(); i++) {
                    multipart.addFormField(myFormDataArray.get(i).getName(),
                            myFormDataArray.get(i).getValue());
                }

                // Files
                for (int i = 0; i < myFileArray.size(); i++) {
                    multipart.addFilePart(String.valueOf(i),
                            myFileArray.get(i).getValue());
                }

                String responseString;
                List<String> response = multipart.finish();
                Log.e(TAG, "SERVER REPLIED:");
                for (String line : response) {
                    Log.e(TAG, "Upload Files Response:::" + line);
                    // get your server response here.
                    responseString = line;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if (result) {
                // Clear PhotoFragment
                photoFragment.clearItems();
                myFileArray.clear();

                // Clear AssistanceDetailsFragment
                mCurrentPhotoPath = "";
                bitmap = null;
                assistanceDetailsFragment.clear();
            }
        }
    }

    public class AsyncTaskPhotoChalkBoard extends AsyncTask {
        private static final String TAG = "atPhotoChalkBoard";

        @Override
        protected Object doInBackground(Object[] params) {
            MultipartUtility multipart = null;
            try {
                // ?action=foto_pizarra&galeria_id=1
                // $_FILES[foto]
                multipart = new MultipartUtility(URL_ASSISTANCE, "utf-8");

                // Post
                List<MyPost> myFormDataArray = new ArrayList<MyPost>();

                myFormDataArray.add(new MyPost("action", "foto_pizarra"));
                myFormDataArray.add(new MyPost("galeria_id", gallery_id));

                for (int i = 0; i < myFormDataArray.size(); i++) {
                    multipart.addFormField(myFormDataArray.get(i).getName(),
                            myFormDataArray.get(i).getValue());
                }

                if (myFile != null)
                    multipart.addFilePart(myFile.getName(),
                            myFile.getValue());

                String responseString = "";
                List<String> response = multipart.finish();
                Log.e(TAG, "SERVER REPLIED:");
                for (String line : response) {
                    Log.e(TAG, "Upload Files Response:::" + line);
                    // get your server response here.
                    responseString = line;
                }

                // JSON
                try {
                    // {"results":[{"ok":1}],"success":1}
                    JSONObject obj = new JSONObject(responseString);

                    if (obj.getInt("success") == 1) {
                        JSONArray dataArray = obj.getJSONArray("results");
                        JSONObject row = dataArray.getJSONObject(0);
                        row.getString("ok");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    public class AsyncTaskEmail extends AsyncTask<Void, Void, Boolean> {
        private static final String TAG = "atEmail";

        @Override
        protected Boolean doInBackground(Void... params) {
            MultipartUtility multipart = null;
            try {
                // ?action=email&id_taller=1&f_ini=2016-05-11
                multipart = new MultipartUtility(URL_ASSISTANCE, "utf-8");

                // Post
                List<MyPost> myFormDataArray = new ArrayList<MyPost>();

                myFormDataArray.add(new MyPost("action", "email"));
                myFormDataArray.add(new MyPost("id_taller", workshop_id));
                myFormDataArray.add(new MyPost("f_ini", workshop_date));

                for (int i = 0; i < myFormDataArray.size(); i++) {
                    multipart.addFormField(myFormDataArray.get(i).getName(),
                            myFormDataArray.get(i).getValue());
                }

                // Files
                for (int i = 0; i < myFileArray.size(); i++) {
                    multipart.addFilePart(myFileArray.get(i).getName(),
                            myFileArray.get(i).getValue());
                }

                String responseString;
                List<String> response = multipart.finish();
                Log.e(TAG, "SERVER REPLIED:");
                for (String line : response) {
                    Log.e(TAG, "Upload Files Response:::" + line);
                    // get your server response here.
                    responseString = line;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if (result) {
                Toast.makeText(AssistanceActivity.this, R.string.assistance_send, Toast.LENGTH_LONG).show();
                goToHome();
            }
        }
    }

    @Override
    public void onListFragmentInteraction(PersonContent.PersonItem item) {
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    public void onListFragmentInteraction(PhotoContent.PhotoItem item) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    assistanceDetailsFragment = AssistanceDetailsFragment.newInstance(workshop_id);

                    if (!mCurrentPhotoPath.isEmpty() && bitmap != null)
                        assistanceDetailsFragment.setPhoto(bitmap);

                    return assistanceDetailsFragment;
                case 1:
                    personFragment = PersonFragment.newInstance(workshop_id, "1");
                    return personFragment;
                default:
                    photoFragment = PhotoFragment.newInstance(2);
                    return photoFragment;
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.assistance_details);
                case 1:
                    return getString(R.string.assistance_assists);
                case 2:
                    return getString(R.string.assistance_gallery);
            }
            return null;
        }
    }
}
