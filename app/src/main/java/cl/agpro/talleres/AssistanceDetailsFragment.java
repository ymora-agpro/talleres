package cl.agpro.talleres;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AssistanceDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AssistanceDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AssistanceDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private static final String TAG = "aDetailsFragment";
    private AssistanceActivity assistanceActivity;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private getData getData;
    private View rootView;

    int mYear, mMonth, mDay;

    private ImageView img_workshop;
    private ImageButton btn_camera;
    private Button btn_date;
    private TextView txt_date;
    private EditText txt_observation;

    private TextView txt_type, txt_teams, txt_gender, txt_age, txt_technique, txt_dateClass;

    private OnFragmentInteractionListener mListener;
    private MyActivity myActivity;

    private Boolean loaded = false;

    public static final AssistanceDetails assistanceDetails = new AssistanceDetails();

    public AssistanceDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.myActivity = (MyActivity) context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment AssistanceDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AssistanceDetailsFragment newInstance(String param1) {
        AssistanceDetailsFragment fragment = new AssistanceDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }

        Log.d("asd", mParam1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_assistance_details, container, false);
        img_workshop = (ImageView) rootView.findViewById(R.id.imgWorkshop);
        btn_camera = (ImageButton) rootView.findViewById(R.id.btnCamera);
        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myActivity.mayRequestCamera(v, MyActivity.REQUEST_CHALKBOARD);
            }
        });
        txt_date = (TextView) rootView.findViewById(R.id.lblDateArriendo);
        btn_date = (Button) rootView.findViewById(R.id.btn_date);
        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDatePicker();
            }
        });
        txt_observation = (EditText) rootView.findViewById(R.id.txtObservationWorkshop);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        txt_type = (TextView) rootView.findViewById(R.id.txtNameWorkshop);
        txt_teams = (TextView) rootView.findViewById(R.id.txtTeacherWorkshop);
        txt_gender = (TextView) rootView.findViewById(R.id.txtDateStartWorkshop);
        txt_age = (TextView) rootView.findViewById(R.id.txtSubsidiaryWorkshop);
        txt_technique = (TextView) rootView.findViewById(R.id.txtTechniqueWorkshop);

        txt_dateClass = (TextView) rootView.findViewById(R.id.lblDateArriendo);

        if (loaded) {
            setDetails();
        } else {
            getData = new getData();
            getData.execute();
        }

        return rootView;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class getData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String Lang = Locale.getDefault().getDisplayLanguage();
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", assistanceActivity.URL_WS + "/get_data_taller.php?taller_id=" + mParam1);
                url = new URL(assistanceActivity.URL_WS + "/get_data_taller.php?taller_id=" + mParam1);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;
            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResults(result);
        }
    }

    public void getResults(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("taller");

            JSONObject row = dataArray.getJSONObject(0);

            assistanceDetails.name = row.getString("nombre");
            assistanceDetails.teacher = row.getString("profe");
            assistanceDetails.dateStart = row.getString("fecha");
            assistanceDetails.subsidiary = row.getString("sucursal");
            assistanceDetails.technique = row.getString("tecnica");

            setDetails();

            loaded = true;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setDetails() {
        // Server
        txt_type.setText(assistanceDetails.name);
        txt_teams.setText(assistanceDetails.teacher);
        txt_gender.setText(assistanceDetails.dateStart);
        txt_age.setText(assistanceDetails.subsidiary);
        txt_technique.setText(assistanceDetails.technique);

        // Local
        if (assistanceDetails.chalkBoardPhoto != null)
            img_workshop.setImageBitmap(assistanceDetails.chalkBoardPhoto);

        if (assistanceDetails.dateClass != null)
            txt_dateClass.setText(assistanceDetails.dateClass);
    }

    public String getObservation() {
        return txt_observation.getText().toString();
    }

    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);

        date.show(getFragmentManager(), "Date Picker");

    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {


            String dia, mes, anio;
            dia = String.valueOf(dayOfMonth);
            int len_dia = dia.length();
            if (len_dia == 1) {
                dia = "0" + dia;
            }
            mes = String.valueOf(monthOfYear + 1);
            int len_mes = mes.length();
            if (len_mes == 1) {
                mes = "0" + mes;
            }
            anio = String.valueOf(year);

            Log.d(TAG, anio + "-" + mes + "-" + dia);

            assistanceDetails.dateClass = anio + "-" + mes + "-" + dia;
            txt_date.setText(assistanceDetails.dateClass);
        }
    };

    public String getDate() {
        return assistanceDetails.dateClass;
    }

    public void setPhoto(Bitmap bitmap) {
        img_workshop.setImageBitmap(bitmap);
        assistanceDetails.chalkBoardPhoto = bitmap;
    }

    public void clear() {
        assistanceDetails.dateClass = null;
        assistanceDetails.chalkBoardPhoto = null;
        img_workshop.setImageBitmap(null);
    }

    public static class AssistanceDetails {
        public Integer id;

        public String name;
        public String teacher;
        public String dateStart;
        public String subsidiary;

        public Bitmap chalkBoardPhoto = null;

        public String technique;
        public String dateClass = null;
        public String observations;

        public AssistanceDetails() {
        }
    }
}
