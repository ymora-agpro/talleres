package cl.agpro.talleres;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends MyActivity {

    private static final int TIEMPO = 3000;
    public String TAG = "MainActivity";
    SharedPreferences prefs;
    String type;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = getSharedPreferences("usuario", Context.MODE_PRIVATE);
        id = prefs.getInt("id", 0);
        type = prefs.getString("type", "");

        View view = findViewById(R.id.fullscreen_content);
        hide(view);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (id > 0) {
                    new GetData().execute();
                } else {
                    goToWelcome();
                }
            }
        }, TIEMPO);
    }

    private class GetData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            StringBuilder response = new StringBuilder();

            URL url = null;
            try {
                Log.d("URL ", URL_WS + "/get_validate_session.php?user_id=" + id + "&type=" + type);
                url = new URL(URL_WS + "/get_validate_session.php?user_id=" + id + "&type=" + type);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpURLConnection httpconn = null;

            try {
                httpconn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()), 8192);
                    String strLine = null;
                    while ((strLine = input.readLine()) != null) {
                        response.append(strLine);
                    }
                    response.append(strLine);
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            getResult(result);
        }
    }

    public void getResult(String response) {
        JSONObject obj = null;
        try {
            //RESPUESTA DE URL
            obj = new JSONObject(response.toString());
            //OBTENGO EL ARRAY DENTRO DEL OBJETO
            JSONArray dataArray = obj.getJSONArray("validate");
            JSONObject row = dataArray.getJSONObject(0);
            int val = row.getInt("ok");
            if (val == 1) {
                if (type.equals("admin")) {
                    Log.d(TAG, "Es administrador");
                    goToHome();
                } else if (type.equals("caja")) {
                    Log.d(TAG, "Es de la caja");
                    goToHome();
                } else {
                    Log.d(TAG, "Es profesor");
                    goToHomeProfe();
                }
            } else {
                goToWelcome();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
