package cl.agpro.talleres;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import cl.agpro.talleres.person.Person;
import cl.agpro.talleres.person.PersonContent;
import cl.agpro.talleres.person.Person.PersonItem;
import cl.agpro.talleres.person.RegisterContent;
import cl.agpro.talleres.person.RegisteredContent;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class PersonFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Customize parameters
    private int mColumnCount = 1;

    private OnListFragmentInteractionListener mListener;

    private static final String TAG = "PersonFragment";
    private String workshop_id, type;
    private View rootView;

    private PersonContent personContent;
    private RegisterContent registerContent;
    private RegisteredContent registeredContent;

    private MyPersonRecyclerViewAdapter myPersonRecyclerViewAdapterRegister;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PersonFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PersonFragment newInstance(String workshop_id, String type) {
        PersonFragment fragment = new PersonFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, workshop_id);
        args.putString(ARG_PARAM2, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            workshop_id = getArguments().getString(ARG_PARAM1);
            type = getArguments().getString(ARG_PARAM2);
        }
        Log.d("Workshop ID", workshop_id);
        Log.d("Type", type);
    }

    @Override
    public void onResume() {
        super.onResume();

        switch (type) {
            case "1":
                if (personContent.ITEMS_P.size() > 0) {
                    personContent.ITEMS_P.clear();
                    personContent.getDataAssistance();
                }
                break;
            case "2":
                if (registerContent.ITEMS_RT.size() > 0) {
                    registerContent.ITEMS_RT.clear();
                    registerContent.getDataRegister();
                }
                break;
            default:
                if (registeredContent.ITEMS_RD.size() > 0) {
                    registeredContent.ITEMS_RD.clear();
                    registeredContent.getDataRegistered();
                }
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_person_list, container, false);

        // Set the adapter
        if (rootView instanceof RecyclerView) {
            Context context = rootView.getContext();
            RecyclerView recyclerView = (RecyclerView) rootView;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            switch (type) {
                case "1":
                    personContent = new PersonContent(workshop_id, type);
                    final MyPersonRecyclerViewAdapter myPersonRecyclerViewAdapter = new MyPersonRecyclerViewAdapter(personContent.ITEMS_P, mListener, type, workshop_id);

                    personContent.setOnListFragmentInteractionListener(new Person.OnListFragmentInteractionListener() {
                        @Override
                        public void onListFragmentInteraction() {
                            myPersonRecyclerViewAdapter.notifyDataSetChanged();
                        }
                    });

                    recyclerView.setAdapter(myPersonRecyclerViewAdapter);
                    break;
                case "2":
                    registerContent = new RegisterContent(workshop_id, type);
                    myPersonRecyclerViewAdapterRegister = new MyPersonRecyclerViewAdapter(registerContent.ITEMS_RT, mListener, type, workshop_id);

                    registerContent.setOnListFragmentInteractionListener(new Person.OnListFragmentInteractionListener() {
                        @Override
                        public void onListFragmentInteraction() {
                            myPersonRecyclerViewAdapterRegister.notifyDataSetChanged();
                        }
                    });

                    recyclerView.setAdapter(myPersonRecyclerViewAdapterRegister);
                    break;
                default:
                    registeredContent = new RegisteredContent(workshop_id, type);
                    final MyPersonRecyclerViewAdapter myPersonRecyclerViewAdapter3 = new MyPersonRecyclerViewAdapter(registeredContent.ITEMS_RD, mListener, type, workshop_id);

                    registeredContent.setOnListFragmentInteractionListener(new Person.OnListFragmentInteractionListener() {
                        @Override
                        public void onListFragmentInteraction() {
                            myPersonRecyclerViewAdapter3.notifyDataSetChanged();
                        }
                    });

                    recyclerView.setAdapter(myPersonRecyclerViewAdapter3);
                    break;
            }
        }


        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } /*else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public List<PersonItem> getPersons() {
        switch (type) {
            case "1":
                return personContent.ITEMS_P;
            case "2":
                return registerContent.ITEMS_RT;
            default:
                return registeredContent.ITEMS_RD;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(PersonItem item);
    }

    public void setOnListFragmentInteractionListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }

    public void setDataRegister(String searchText) {

        registerContent.ITEMS_RT.clear();

        for (PersonItem seguimiento : registerContent.ITEMS_RT_ALL)
            if (Pattern.compile(Pattern.quote(searchText), Pattern.CASE_INSENSITIVE).matcher(seguimiento.name).find())
                registerContent.ITEMS_RT.add(seguimiento);

        myPersonRecyclerViewAdapterRegister.notifyDataSetChanged();
    }

    public void setDataClear() {
        registerContent.ITEMS_RT.clear();

        registerContent.ITEMS_RT.addAll(registerContent.ITEMS_RT_ALL);

        myPersonRecyclerViewAdapterRegister.notifyDataSetChanged();
    }
}
